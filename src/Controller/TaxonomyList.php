<?php

namespace Drupal\cleantaxonomy\Controller;

use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for the cleantaxonomy module.
 */
class TaxonomyList extends ControllerBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Constructs a cleantaxonomy object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function TaxonomyTermsList() {
    $header = [
      $this->t('Term ID'),
      $this->t('Taxonomy Term'),
      $this->t('Vocabulary'),
      [
        'data' => $this->t('No. of nodes attached'),
        'sort' => 'desc'
      ],
      [
        'data' => $this->t('Operation'),
        'colspan' => '4'
      ],
    ];
    $id_name = \Drupal::database()->select('taxonomy_term_field_data')
      ->fields('taxonomy_term_field_data',['tid','name', 'vid'])
      ->execute();
    $id_name_records=$id_name->fetchAll();
    $id_name_values = [];
    $size=0;
    foreach ($id_name_records as $id_name_record) {
      $id_name_values[$id_name_record->tid] = $id_name_record;
      $size++;
    }
    $node_count= \Drupal::database()->query("SELECT count(nid) AS n,tid FROM {taxonomy_index} GROUP BY tid",[]);
    $node_count_records=$node_count->fetchAll();
    $i=0;
    foreach ($node_count_records as $node_count_record) {
      $tid_value[$i]= $node_count_record->tid;
      $nid_count_value[$i]= $node_count_record->n;
      $i++;
    }
    $j=0;
    for ($i=1; $i <=$size; $i++) {
      if($tid_value[$j]!=$i)  {
        $nodes[$i]=0;
      }
      else {
        $nodes[$i]=$nid_count_value[$j];
        $j++;
      }
    }
    $s=1;
    foreach ($id_name_values as $id_name_value) {
      $tid[$s]=$id_name_value->tid;
      $name[$s]=$id_name_value->name;
      $vocab[$s]=$id_name_value->vid;
      $s++;
    }

    $s=1;
    $rows = [];
    for($n=1;$n<=$size;$n++) {
      if($vocab[$n]=='tags') {
        $rows[] = [
            'data' => [
              $this->t($tid[$n]),
              $this->t($name[$n]),
              $this->t($vocab[$n]),
               print_r($nodes[$s],$s++),
              \Drupal::l($this->t('View'), Url::fromUri('internal:/taxonomy/term/'.$tid[$n],[$tid[$n]])),
              \Drupal::l($this->t('Edit'), Url::fromUri('internal:/taxonomy/term/'.$tid[$n].'/edit')),
              \Drupal::l($this->t('Delete'), Url::fromUri('internal:/taxonomy/term/'.$tid[$n].'/delete')),
              \Drupal::l($this->t('Replace'),new Url('cleantaxonomy.admin_cleantaxonomy.tid.replace',['tid'=>$tid[$n]])),
            ],
          ];
      }
      else {
        $rows[] = [
          'data' => [
            $this->t($tid[$n]),
            $this->t($name[$n]),
            $this->t($vocab[$n]),
            print_r($nodes[$s],$s++),
            \Drupal::l($this->t('View'), Url::fromUri('internal:/taxonomy/term/'.$tid[$n],[$tid[$n]])),
            \Drupal::l($this->t('Edit'), Url::fromUri('internal:/taxonomy/term/'.$tid[$n].'/edit')),
            \Drupal::l($this->t('Delete'), Url::fromUri('internal:/taxonomy/term/'.$tid[$n].'/delete')),
          ],
        ];
      }
    }
    $build['admin_cleantaxonomy_list_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No taxonomy terms available.'),
    ];
    $build['admin_cleantaxonomy_list_pager'] = ['#theme' => 'pager'];
    return $build;
  }
}
